import generateMyElement from "./generateElement.js";
import generateCard from "./generateCard.js";


const container=generateMyElement('div',{class:'container'})
container.appendChild(generateCard('ahmad',27));


const userData=[
    {name:'Ali',age:52},
    {name:'Alex',age:26},
    {name:'Sue',age:19}
];

userData.forEach(data => {container.appendChild(generateCard(data.name,data.age))});

export default container;