import generateMyElement  from "./generateElement.js";
function generateCard(name,age){
    const card=generateMyElement('div',{class:'card'},
            generateMyElement('span',null,`name: ${name}`),
            generateMyElement('span',null,`age: ${age}`),
    ) 
    return card;
}

export default generateCard;