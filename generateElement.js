function generateMyElement(tagName,attributes,...childs){
    const elementName=document.createElement(tagName);
    if(attributes){
        Object.keys(attributes).forEach(key=>{
            elementName.setAttribute(key,attributes[key])
        })
    }
    if(childs){
        childs.forEach(child=>{
            if('string'===typeof child){
                elementName.appendChild(document.createTextNode(child))
            }else{
                elementName.appendChild(child);
            }
        })
    }
    return elementName;
}

export default generateMyElement;